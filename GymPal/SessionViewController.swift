//
//  SessionViewController.swift
//  GymPal
//
//  Created by Yanhua Li on 1/02/17.
//  Copyright © 2017 Yanhua Li. All rights reserved.
//

import UIKit


class SessionViewController: UIViewController, UITableViewDataSource {

	@IBOutlet weak var dateLabel: UILabel!
	@IBOutlet weak var tableView: UITableView!
	var session : Session?
	var entries : [Entry]!
	
    override func viewDidLoad() {
        super.viewDidLoad()
		loadEntries()
		dateLabel.text = entityController.dateToString(date: session!.date as! Date, dateStyle: .long)
		
	
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		let destination = segue.destination as! AddEntryViewController
		destination.session = self.session!
	}
	
	func loadEntries() {
		entries = entityController.getAllEntries(session: session!)
}
	
	@IBAction func addEntry(_ sender: Any) {
		performSegue(withIdentifier: "addEntryView", sender: self)
	}
	
	//MARK: UITableViewDataSource
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell : sessionCell = tableView.dequeueReusableCell(withIdentifier: "cell")! as! sessionCell
		let entry = entries[indexPath.row]
		
		cell.comment.text = entry.comment
		cell.exercise.text = entry.exercise
		cell.reps.text = "Reps: " + String(entry.reps)
		cell.weight.text = String(entry.weight) + "kg"
		
		return cell
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return entries.count
	}

	
	func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
		let entryToDelete = entries[indexPath.row]
		if editingStyle == .delete {
			entityController.deleteEntry(entry: entryToDelete)
			loadEntries()
			tableView.reloadData()
		}
	}

}


class sessionCell: UITableViewCell {
	
	@IBOutlet weak var exercise: UILabel!
	@IBOutlet weak var weight: UILabel!
	@IBOutlet weak var reps: UILabel!
	@IBOutlet weak var comment: UILabel!
}
