//
//  AddEntryViewontroller.swift
//  GymPal
//
//  Created by Yanhua Li on 1/02/17.
//  Copyright © 2017 Yanhua Li. All rights reserved.
//

import UIKit
import Eureka

class AddEntryViewController: FormViewController {
	
	var session : Session!

    override func viewDidLoad() {
        super.viewDidLoad()
		
		

		form
			
			+++ Section()
			
			<<< TextRow("exercise"){
				row in
				row.title = "Exercise"
				row.placeholder = "e.g. Bench Press"
				row.add(rule: RuleRequired())
				row.validationOptions = .validatesOnChange
			}
			
			<<< IntRow("weight") {
				row in
				row.title = "Weight"
				row.placeholder = "e.g. 70 (kg)"
				row.add(rule: RuleRequired())
				row.validationOptions = .validatesOnChange
			}
			
			+++ Section("Reps")
		
			<<< PickerRow<Int64>("reps") {
				row in
				row.options = []
				for i in 1...20 {
					row.options.append(Int64(i))
					
				}
				row.value = row.options.first
				
			}
				
			
		
			+++ Section()
		
			<<< TextRow("comment") {
				row in
				row.title = "Comment"
				row.placeholder = "e.g. felt hard"
				row.baseValue = ""
			}
    }

	
	@IBAction func addEntry(_ sender: Any) {
		for row in form.rows {
			_ = row.validate()
		}
		if (form.rowBy(tag: "exercise")?.validationErrors.count)! > 0 {
			let alert = UIAlertController(title: "Error", message: "Please fill in an exercise.", preferredStyle: .alert)
			alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
			present(alert, animated: true, completion: nil)
			return
		}
		if (form.rowBy(tag: "weight")?.validationErrors.count)! > 0 {
			let alert = UIAlertController(title: "Error", message: "Please fill in weight", preferredStyle: .alert)
			alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
			present(alert, animated: true, completion: nil)
			return
		}
		
		
		let values = form.values()
		let exercise = values["exercise"] as! String
		let weight = Double(values["weight"] as! Int)
		let comment = values["comment"] as! String
		let reps = values["reps"] as! Int64

		
		
		entityController.addNewEntry(session: self.session, exercise: exercise, weight: weight, reps: reps, comment: comment)
		
		
		
		
		
		//we will update the tableview of the previous controller
		let VCCount = self.navigationController?.viewControllers.count
		let previousVC = self.navigationController?.viewControllers[VCCount! - 2] as! SessionViewController
		previousVC.loadEntries()
		previousVC.tableView.reloadData()
		
		_ = navigationController?.popViewController(animated: true)

		
		
	}
	
	
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
