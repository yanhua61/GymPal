//
//  EntityController.swift
//  GymPal
//
//  Created by Yanhua Li on 1/02/17.
//  Copyright © 2017 Yanhua Li. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class EntityController {
	
	var appDelegate : AppDelegate
	var context : NSManagedObjectContext
	
	init() {
		appDelegate = UIApplication.shared.delegate as! AppDelegate
		context = appDelegate.persistentContainer.viewContext
	}
	
	//MARK: Session
	
	func dateToString(date: Date, dateStyle: DateFormatter.Style) -> String {
		let myFormatter = DateFormatter()
		
		myFormatter.dateStyle = dateStyle
		
		let formattedDate = myFormatter.string(from: date )
		
		return formattedDate
	}
	
	func getAllSessions() -> [Session] {
		
		var sessions : [Session] = []
		let fetchRequest = Session.fetchRequest() as NSFetchRequest<Session>
		
		do {
			sessions = try context.fetch(fetchRequest)
		} catch {
			print("Error while trying to fetch sessions")
		}
		
		return sessions
	}
	
	func deleteAllSessions() {
		var sessions : [Session] = []
		let fetchRequest = Session.fetchRequest() as NSFetchRequest<Session>
		
		do {
			sessions = try context.fetch(fetchRequest)
		} catch {
			print("Error while trying to fetch sessions")
		}
		
		for session in sessions {
			context.delete(session)
		}
		
		do {
			try context.save()
		} catch {
			print("Error while trying to save")
		}
	}
	
	func addNewSession() {
		let session = Session(context: context)
		
		session.date = Date() as NSDate?
		
		do {
			try context.save()
		} catch {
			print("Error while trying to save sessions")
		}
	}
	
	func deleteSession(session : NSManagedObject) {
		context.delete(session)
		
		do {
			try context.save()
		} catch {
			print("Error while trying to save")
		}

	}
	

	
	
	//MARK: Entry
	
	
	func addNewEntry(session: Session, exercise: String, weight: Double, reps: Int64, comment: String) {
		let entry = Entry(context: context)
		
		entry.exercise = exercise
		entry.weight = weight
		entry.reps = reps
		entry.comment = comment
		
		entry.session = session
		
		do {
			try context.save()
		} catch {
			print("Error while trying to sae entry")
		}
		
	}
	
	func deleteEntry(entry: NSManagedObject) {
		
		context.delete(entry)
		
		
		do {
			try context.save()
		} catch {
			print("Error while trying to save")
		}

	}
	
	
	func getAllEntries(session: Session)  -> [Entry]{
		let entries = session.entries?.allObjects as! [Entry]
		return entries
		
	}
}
