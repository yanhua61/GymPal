//
//  ViewController.swift
//  GymPal
//
//  Created by Yanhua Li on 9/01/17.
//  Copyright © 2017 Yanhua Li. All rights reserved.
//

import UIKit


class MainViewController: UIViewController {
	
	var sessions : [Session] = []
	var sessionToPass : Session?

	@IBOutlet weak var tableView: UITableView!
	
	
	override func viewDidLoad() {
		super.viewDidLoad()
		loadSessions()
		// Do any additional setup after loading the view, typically from a nib.
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	func loadSessions() {
		
		sessions = entityController.getAllSessions()
		
	}
	

	@IBAction func addNewSession(_ sender: Any) {
		
		
		entityController.addNewSession()
		loadSessions()
		tableView.reloadData()
		
		
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		let destination = segue.destination as! SessionViewController
		destination.session = sessionToPass
		
	}
	

}

extension MainViewController: UITableViewDelegate, UITableViewDataSource {
	
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
		let session = sessions[indexPath.row]
		
		
		cell.textLabel?.text = entityController.dateToString(date: session.date as! Date, dateStyle: .full)
		
		
		return cell

	}
	
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return sessions.count
	}
	
	//swipe to delete functionality
	func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
		if editingStyle == .delete {
			let sessionToDelete = sessions[indexPath.row]
			
			entityController.deleteSession(session: sessionToDelete)
			loadSessions()
			tableView.reloadData()
			
		}
	}
	
	//go to individual session view when a cell is tapped
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		sessionToPass = sessions[indexPath.row]
		performSegue(withIdentifier: "sessionView", sender: self)
	}
	
	
	
	
}

